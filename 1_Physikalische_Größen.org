# -*- coding: utf-8 -*-
#+TITLE:     PharmBasics (N) – Physikalische Größen
#+AUTHOR:    Prof. Dr. Richard Hirsch
#+EMAIL:     richard.hirsch@th-koeln.de
#+DATE:      Wintersemester 2022/23
#+LaTeX_HEADER: \institute{Pharmazeutische Chemie}

#+DESCRIPTION: Richard's lectures on statistics
#+KEYWORDS: 
#+LANGUAGE:  de

* Beamer configuration                                             :noexport:

#+startup: beamer
#+OPTIONS: H:3

** Latex

#+LaTeX_CLASS: lecture
#+LaTeX_CLASS_OPTIONS: [ngerman,detect-all]

#+LaTeX_HEADER: \usepackage{babel}
#+LaTeX_HEADER: \usepackage{accents}
#+LaTeX_HEADER: \usepackage{tikz}
#+LaTeX_HEADER: \usepackage{pgfplots}

#+LaTeX_HEADER: \pgfplotsset{compat=1.16}
#+LaTeX_HEADER: \sisetup{locale=DE}
#+LaTeX_HEADER: \graphicspath{{assets/images/}}

#+LaTeX_HEADER: \newcommand{\entspr}{\mathrel{\accentset{\wedge}{=}}}
#+LaTeX_HEADER: \newcommand{\src}[2]{\par\vspace{-7mm}\hfill{\scriptsize\textsl{#1}~(#2)}}

** TikZ
#+LaTeX_HEADER: \tikzstyle{mengenartig} = [rectangle, minimum width=25mm, minimum height=12mm, align=center, draw]
#+LaTeX_HEADER: \tikzstyle{intensive}   = [above, node font=\footnotesize]
  


* Physikalische Größen
  :PROPERTIES:
  :ALT_TITLE: Phys. Größen
  :END:


** Das Prinzip der physikalischen Größe

*** Physikalische Größen

  Die Ausprägung einer physikalischen Größe wird durch
  *Maßzahl* und *Maßeinheit* charakterisiert:

#+BEGIN_CENTER
  \bigskip
    \begin{tabular}{c@{${}={}$}c@{${}\cdot{}$}c}
      Größen-Wert & Maß-Zahl & Maß-Einheit\\
      $G$         &  $\{G\}$ &    $[G]$
    \end{tabular}
#+END_CENTER

**** Temperatur                                              :B_example:
     :PROPERTIES:
     :BEAMER_env: example
     :END:
\begin{align*}
     m     &= \qty{273.15}{\celsius}\\
     \{m\} &= \num{273.15}\\
     [m]   &= \hphantom{\num{273.15}\,}\unit{\celsius}
\end{align*}



*** Richtlinien

- Bureau International des Poids et Mesures: /[[https://www.bipm.org/en/publications/si-brochure/][SI Brochure: The International System of Units (SI)]]/, 2019
- A. Thompson and B. N. Taylor, [[https://www.nist.gov/pml/special-publication-811][/NIST Guide to the SI/]], [[https://www.nist.gov/pml/special-publication-811/nist-guide-si-chapter-7-rules-and-style-conventions-expressing-values][section 7.1]], Special Publication 811, 2008
- IUPAC guide /Symbols, units, nomenclature and fundamental constans in physics/,
- [[https://www.iso.org/obp/ui/#iso:std:iso:80000:-1:ed-1:v1:en][ISO 80000-1:2009, /Quantities and units — Part 1: General/]], section 3.20
- IUPAC /Quantities, Units and Symbols in Physical Chemistry/, third ed., 
  RSC Publishing, Cambridge 2007, ISBN: 978-0-85404-433-7


*** Rechnen mit physikalischen Größen

Mit physikalischen Größen rechnet man genauso wie mit
algebraischen Produkten.

**** Multiplikation                                               :B_example:
     :PROPERTIES:
     :BEAMER_env: example
     :END:
\begin{align*}
    \SI{60}{\watt} \cdot \SI{24}{\hour}
      &= (60\cdot24) \cdot (\si{\watt}\cdot\si{\hour})\\
      &= \num{1.440e3} \cdot \si{\watt\hour}\\
      &= \num{1.440} \cdot \si{\kilo\watt\hour}\\
      &= \num{1.440} \cdot \si{\kilo\watt}\cdot\SI{3600}{\second}\\
      &= \num{5.184e3} \cdot \si{\kilo\watt\second}\\
      &= \SI{5.184}{\mega\joule}
\end{align*}

*** Rechnen mit physikalischen Größen

**** Addition                                                     :B_example:
     :PROPERTIES:
     :BEAMER_env: example
     :END:
  \begin{equation*}
  \SI{500}{\gram} + \SI{20}{\gram} = (500 + 20)\si{\gram} = \SI{520}{\gram}
  \end{equation*}


\pause\bigskip

**** FALSCH :B_example:
     :PROPERTIES:
     :BEAMER_env: example
     :END:

  \begin{equation*}
  \SI{500}{\gram} + \SI{20}{\metre} = \bot
  \end{equation*}

*Größen unterschiedlicher Dimension kann man nicht addieren!*

*** Addition von Größen gleicher Dimension

**** Beispiel :B_exampleblock:
     :PROPERTIES:
     :BEAMER_env: exampleblock
     :END:

Ergibt dieser Ausdruck einen Sinn?

#+BEGIN_EXPORT latex
\begin{equation*}
  \qty{5}{\cm} + \qty{2}{in} = \uncover<4>{\color{THKred}\qty{10.08}{\cm}}
\end{equation*}
\pause
\begin{center}
  \begin{tikzpicture}
    \draw[|-|] (0, 0) -- node[above]{\qty{5}{\cm}} (5, 0);
    \draw[|-|] (5, 0) -- node[above]{\qty{2}{in}} (10.08, 0);
    \pause
    \draw[|-|] (0, -1) -- node[above]{\qty{5}{\cm}} (5, -1);
    \draw[|-|] (5, -1) -- node[above]{\qty{5.08}{\cm}} (10.08, -1);
    \pause
    \draw[|-|] (0, -2) -- node[above]{\qty{10.08}{\centi\metre}} (10.08, -2);
  \end{tikzpicture}
\end{center}
#+END_EXPORT


*** Umrechnung von Größen gleicher Dimension

**** Umrechnung von Inch in Zentimeter                            :B_example:
     :PROPERTIES:
     :BEAMER_env: example
     :END:
  \begin{equation*}
  \SI{1}{in} = \SI{2.54}{\centi\metre} \qquad \Leftrightarrow \qquad
  \frac{\SI{2.54}{\centi\metre}}{\SI{1}{in}} = 1
  \end{equation*}

\pause\medskip

**** Addition:                                                    :B_example:
     :PROPERTIES:
     :BEAMER_env: example
     :END:
  \begin{align*}
  \SI{5}{\centi\metre} + \SI{2}{in}
    &= \SI{5}{\centi\metre}
      + \SI{2}{in}
      \cdot\underbrace{\frac{\SI{2.54}{\centi\metre}}{\SI{1}{in}}}_{1}\\
    &= \SI{5.00}{\centi\metre} + \SI{5.08}{\centi\metre}\\
    &= \SI{10.08}{\centi\metre}
  \end{align*}






** Système international d'unités

*** Le système international d'unités

- [[https://www.bipm.org/en/publications/si-brochure/][Internationales Einheitensystem]], auf der ganzen Welt verbreitet
- Stand von Wissenschaft und Technik
- Ursprung in der französischen Revolution
  + Grundheinheiten aus naturgegebenen Größen abgeleitet, \newline(aber anthropistisch),
  + weitere Größen aus den Grundeinheiten abgeleitet
  + dezimale Vervielfachung oder Unterteilung

\pause
In Deutschland nach dem [[https://www.gesetze-im-internet.de/me_einhg/index.html][Einheiten- und Zeitgesetz]] für den geschäftlichen umd amtlichen Verkehr vorgeschrieben.

*** SI-Basiseinheiten

Jede physikalische Größe kann auf ein Produkt von sieben *Basisgrößen*
zurückgeführt werden:
\pause

| Basisgröße  | Symbol | Einheit   | Einheitenzeichen |
|-------------+--------+-----------+------------------|
| Länge       | $l, s$ | Meter     | \si{\metre}      |
| Masse       | $m$    | Kilogramm | \si{\kilo\gram}  |
| Zeit        | $t$    | Sekunde   | \si{\second}     |
| Stromstärke | $I$    | Ampere    | \si{\ampere}     |
| Temperatur  | $T$    | Kelvin    | \si{\kelvin}     |
| Stoffmenge  | $n$    | Mol       | \si{\mole}       |
| Lichtstärke | $I_V$  | Candela   | \si{\candela}    |


*** Abgeleitete Größen (Beispiele)

| Größe           | Symbol | Einheit |
|-----------------+--------+---------|
| Geschwindigkeit | $v$    | \unit{\metre\per\second} |


*** Abgeleitete Einheiten

Die SI-Einheit einer physikalischen Größe $Q$ kann als (Potenz-)Produkt
(und ggf. einem numerischen Faktor) ausgedrückt werden:

\begin{gather*}
 [Q] = 10^n \cdot \si{\metre}^i
              \cdot \si{\kilo\gram}^j
              \cdot \si{\second}^k
              \cdot \si{\ampere}^l
              \cdot \si{\kelvin}^m
              \cdot \si{\mole}^o
              \cdot \si{\candela}^p\\
   i, j, k, l, m, n, o, p \in \mathbb{Z} 
\end{gather*}


**** abgeleitete Einheiten                                        :B_example:
     :PROPERTIES:
     :BEAMER_env: example
     :END:

| Größe    | Symbol | SI-Einheit                         | abgel. Einheit | Name    |
|----------+--------+------------------------------------+----------------+---------|
| Frequenz | $f$    | \si{\per\second}                   | \si{\hertz}    | Hertz   |
| Kraft    | $F$    | \si{\kilo\gram\metre\per\second\squared} | \si{\newton}   | Newton  |
| Druck    | $p$    | \si{\newton\per\metre\squared}     | \si{\pascal}   | Pascal  |
| Arbeit   | $W$    | \si{\newton\metre}                 | \si{\joule}    | Joule   |
| Leistung | $P$    | \si{\joule\per\second}             | \si{\watt}     | Watt    |
| Ladung   | $Q$    | \si{\ampere\second}                | \si{\coulomb}  | Coulomb |
| Spannung | $U$    | \si{\joule\per\coulomb}            | \si{\volt}     | Volt    |

*** SI-Präfixe

| Potenz  | Name  | Zeichen | Wortherkunft                                        |
|---------+-------+---------+-----------------------------------------------------|
| 10^{1}  | Deka  | da      | griech. /déka/ (zehn)                               |
| 10^{2}  | Hekto | h       | griech. /hekatón/ (hundert)                         |
| 10^{3}  | Kilo  | k       | griech. /chílioi/ (tausend)                         |
| 10^{6}  | Mega  | M       | griech. /mégas/ (groß)                              |
| 10^{9}  | Giga  | G       | griech. /gígas/ (Riese)                             |
| 10^{12} | Tera  | T       | griech. /téras/ (Ungeheuer), griech. /tetrá/ (vier) |
| 10^{15} | Peta  | P       | griech. /pénte/ (fünf)                              |
| 10^{18} | Exa   | E       | griech. /héx/ (sechs)                               |
| 10^{21} | Zetta | Z       | ital. /sette/ (sieben)                              |
| 10^{24} | Yotta | Y       | ital. /otto/ (acht)                                 |


*** SI-Präfixe

| Potenz   | Name  | Zeichen | Wortherkunft             |
|----------+-------+---------+--------------------------|
| 10^{-1}  | Dezi  | d       | lat. /decimus/ (Zehnter) |
| 10^{-2}  | Zenti | c       | lat. /centum/ (hundert)  |
| 10^{-3}  | Milli | m       | lat. /mille/ (tausend)   |
| 10^{-6}  | Mikro | µ       | griech. /mikrós/ (klein) |
| 10^{-9}  | Nano  | n       | griech. /nános/ (Zwerg)  |
| 10^{-12} | Piko  | p       | ital. /piccolo/ (klein)  |
| 10^{-15} | Femto | f       | dän. /femten/ (fünfzehn) |
| 10^{-18} | Atto  | a       | dän. /atten/ (achtzehn)  |
| 10^{-21} | Zepto | z       | lat. /septem/ (sieben)   |
| 10^{-24} | Yokto | y       | lat. /octo/ (acht)       |

*** SI-Präfixe

**** Vorsätze groß                                           :B_column:BMCOL:
     :PROPERTIES:
     :BEAMER_col: 0.4
     :BEAMER_env: column
     :END:

| Potenz  | Name  | Zeichen |
|---------+-------+---------|
| 10^{24} | Yotta | Y       |
| 10^{21} | Zetta | Z       |
| 10^{18} | Exa   | E       |
| 10^{15} | Peta  | P       |
| 10^{12} | Tera  | T       |
| 10^{9}  | Giga  | G       |
| 10^{6}  | Mega  | M       |
| 10^{3}  | Kilo  | k       |
| 10^{2}  | Hekto | h       |
| 10^{1}  | Deka  | da      |

**** Vorsätze klein                                          :B_column:BMCOL:
     :PROPERTIES:
     :BEAMER_col: 0.4
     :BEAMER_env: column
     :END:

| Potenz   | Name  | Zeichen |
|----------+-------+---------|
| 10^{-1}  | Dezi  | d       |
| 10^{-2}  | Zenti | c       |
| 10^{-3}  | Milli | m       |
| 10^{-6}  | Mikro | µ       |
| 10^{-9}  | Nano  | n       |
| 10^{-12} | Piko  | p       |
| 10^{-15} | Femto | f       |
| 10^{-18} | Atto  | a       |
| 10^{-21} | Zepto | z       |
| 10^{-24} | Yokto | y       |


*** Übung: SI-Präfixe                                              :exercise:

\small
| Potenz  | Name  | Zeichen |
|---------+-------+---------|
| 10^{18} | Exa   | E       |
| 10^{15} | Peta  | P       |
| 10^{12} | Tera  | T       |
| 10^{9}  | Giga  | G       |
| 10^{6}  | Mega  | M       |

**** Zeitintervalle

1. Welches Datum hatten wir vor einer Megasekunde? 
2. Welches Datum hatten wir vor einer Gigasekunde?
3. Auf welchem Entwicklungsstand war die Menschheit vor einer Terasekunde?
4. Wie sah die Welt vor einer Petasekunde aus?
5. Das Alter unseres Universums wird auf etwa 14 Milliarden Jahre geschätzt. Geben Sie den Zeitraum
   in SI-Einheiten an. \newline (Welche Genauigkeit ist sinnvoll?)

# 1. vor 11.5 Tagen
# 2. vor 31.7 Jahren
# 3. vor 32 000 Jahren, die Jung-Altsteinzeit hatte gerade begonnen
#                      (Einwanderung des anatomisch modernen Menschen nach Europa)
# 4. vor 32 Millionen Jahren, das Oligozän hatte gerade begonnen. Die Dinosaurier waren seit etwa einer
#                             Petasekunde ausgestorben und Vögel und Säugetiere beherrschten die Welt.
# 5. 4.4 e7 s = 440 Ps = 0.44 Es


*** binäre Vielfache

#+BEGIN_CENTER
#+BEGIN_EXPORT latex
  \begin{tabular}{@{}cllS@{}}
    \toprule
    Symbol & \multicolumn{1}{c}{Name} & \multicolumn{1}{c}{Faktor} & \multicolumn{1}{c}{Abweichung von}\\
           &  &  & \multicolumn{1}{c}{Dezimalpotenz / \unit{\percent}}\\
    \midrule
    Yi & Yobi & \((2^{10})^8 = 2^{80} \approx \num{1,209e24}\) & 21 \\
    Zi & Zebi & \((2^{10})^7 = 2^{70} \approx \num{1,181e21}\) & 18 \\
    Ei & Exbi & \((2^{10})^6 = 2^{60} \approx \num{1,153e18}\) & 15 \\
    Pi & Pebi & \((2^{10})^5 = 2^{50} \approx \num{1,126e15}\) & 13 \\
    Ti & Tebi & \((2^{10})^4 = 2^{40} \approx \num{1,100e12}\) & 10 \\
    Gi & Gibi & \((2^{10})^3 = 2^{30} \approx \num{1,074e09}\) &  7.4  \\
    Mi & Mebi & \((2^{10})^2 = 2^{20} \approx \num{1,049e06}\) &  4.9  \\
    Ki & Kibi & \((2^{10})^1 = 2^{10} \approx \num{1,024e03}\) &  2.4  \\
    \bottomrule
\end{tabular}
#+END_EXPORT
#+END_CENTER

*** Übung: allgemeine Gaskonstante

Für ideale Gase gilt die folgende Zustandsgleichung:
\begin{equation}
p V = nRT
\end{equation}

Wie wir wissen, nimmt ein Mol eines idealen Gases unter
Normalbedingungen (\SI{1013}{\hecto\pascal}, \SI{273}{\kelvin}) ein
Volumen von \SI{22.4}{\litre} ein.

\medskip
1. Welche Dimension haben die beiden Seiten der Zustandsgleichung (1)?
2. Welchen Wert hat die allgemeine Gaskonstante?



*** Schriftsatz

[[https://www.bipm.org/en/publications/si-brochure/][SI Brochure: The International System of Units (SI)]] Abschnitt 5:

- Einheitensymbole werden aufrecht gesetzt (nicht kursiv),
  \newline unabhängig vom umgebenden Text. 
- Einheitensymbole werden klein geschrieben.
  + Ausnahme: Der Name ist von einer Person abgeleitet.
  + Ausnahme: Für das Liter ist auch das Einheitensymbol L erlaubt.
- Einheitensymbole werden im Plural nicht verändert.
- Nach den Einheitensymbolen wird, außer am Satzende, kein Punkt gesetzt

- Maßzahl und Maßeinheit werden durch einen kleinen Zwischenraum getrennt (*kein Zeilenumbruch!*)
  + Ausnahme: Winkelangaben: \qty{20.25}{\degree}


*** COMMENT Beispiele

#+ATTR_LATEX: :booktabs

| SI-konform                                   | nicht konform                                           |
|----------------------------------------------+---------------------------------------------------------|
| \qty{1.00}{\metre}, \qty[output-decimal-marker={.}, {1.00}{\metre}, \num{1.00} \unit{\metre} | \num{1.00}\unit{\metre}                                 |
| \qty{22}{\celsius}                           | \num{22}\unit{\celsius}, \color{THKred}\qty{295.13}{\degree}K       |
| \qty{80}{\percent}                           | \num{80}\unit{\percent}                                 |
| \qty{20.25}{\degree}                         | \num{20.25}\thinspace\unit{\degree}                     |
|-
| \qty{101300}{\pascal}, \qty{1013}{\hecto\pascal}, \qty{0.103}{\mega\pascal} | \color{THKred}.103\thinspace MPa    |
| \qty{1.013e5}{\pascal}, \qty[exponent-product=\times]{1.013e5}{\pascal}, 
| \qty[per-mode=index]{8.3}{\joule\per\mole\per\kelvin}, \qty[per-mode=fraction]{8.3}{\joule\per\mole\per\kelvin}, \qty[per-mode=symbol]{8.3}{\joule\per\mole\per\kelvin} |
|----------------------------------------------+---------------------------------------------------------|
| \qty{2}{l}, \qty{2}{L}                       | \unit{2}{$\,\ell$}                                      |
| \qty{1}{\micro\metre}                        | \num{1} $\mu\mbox{m}$, \qty{1}{um}, \color{THKred}\qty{1}{mcm}       |
|----------------------------------------------+---------------------------------------------------------|

*** Beispiele

#+BEGIN_EXPORT latex
\begin{center}
\small
\begin{tabular}{@{}ll@{}}
  \toprule
  SI-konform & nicht konform  \\
  \midrule
  \qty{1.00}{\metre}, \qty[output-decimal-marker={.}]{1.00}{\metre}, \num{1.00} \unit{\metre} &
  \num{1.00}\unit{\metre} \\
  \qty{22}{\celsius}    & \num{22}\unit{\celsius}, \color<2>{THKred}\qty{295.13}{\degree}K \\
  \qty{80}{\percent}    & \num{80}\unit{\percent}   \\
  \qty{20.25}{\degree}  & \num{20.25}\thinspace\unit{\degree} \\
  \midrule
  \qty{101300}{\pascal}, \qty{1013}{\hecto\pascal}, \qty{0.103}{\mega\pascal}
    & \color<2>{THKred}.103\thinspace MPa, 101{.}{300}{,}0\,Pa \\
  \qty{1.013e5}{\pascal}, \qty[exponent-product=\times]{1.013e5}{\pascal} \\
  \qty[per-mode=power]{8.3}{\joule\per\mole\per\kelvin},
  \qty[per-mode=fraction]{8.3}{\joule\per\mole\per\kelvin},
  \qty[per-mode=symbol]{8.3}{\joule\per\mole\per\kelvin} \\
  \midrule
  \qty{2}{l}, \qty{2}{L} & \unit{2}{$\,\ell$}                                             \\
  \qty{1}{\micro\metre}  & \num{1} $\mu\mbox{m}$, \qty{1}{um}, \color<2>{THKred}\qty{1}{mcm} \\
  \bottomrule
\end{tabular}
\end{center}
#+END_EXPORT


*** SI-Präfixe

- Einheitenpräfixe werden aufrecht gesetzt (nicht kursiv).
- Zwischen Präfix und Einheitenzeichen wird kein Zwischenraum geschrieben.
- Es wird maximal ein Präfix pro Einheit verwendet.
- Für Zeiteinheiten außerhalb des /SI/ (Minute (min), Stunde (h), Tag (d)) werden Präfixe nicht verwendet.
- Bei einer Potenzierung gilt der Exponent für das Präfix mit:
  $\qty{5}{\kilo\metre\squared} = 5 \cdot (\qty{1000}{\metre})^2 = \qty{5000000}{\metre\squared}$
- Bei zusammengestzten Einheiten steht m für Meter immer an letzter
  Stelle; ein vorangestelltes m wird als Präfix interpretiert (\unit{\newton\metre}, \unit{\milli\pascal\second})

*** Tabellenköpfe und Koordinatenachsen

\begin{equation*}
  \frac{\qty{60.0}{\kg}}{\unit{\kg}} = 60.0
\end{equation*}

**** Tabellen

In den Spalten wird die Maßzahl $\{G\}$ angegeben, 
\newline im Kopf muss daher $G/[G]$ stehen:

#+BEGIN_CENTER
#+BEGIN_EXPORT latex
\begin{tabular}{@{}ccr@{\thinspace}l@{}}
\toprule
\(m/\si{\kilo\gram}\) & \(\mbox{Länge}/\si{\centi\metre}\) & \multicolumn{2}{c}{Dauer} \\
\midrule
60,0 & 14,4 & 604800 & \unit{\second} \\
64,2 & 18,3 &  10080 & \unit{\minute} \\
80,5 & 20,7 &    168 & \unit{\hour}   \\
93,2 & 12,9 &      7 & \unit{\day}    \\
\bottomrule
\end{tabular}
#+END_EXPORT
#+END_CENTER

*** Tabellenköpfe und Koordinatenachsen
**** Koordinatenachsen

Auf den Achsen wird die Maßzahl $\{G\}$ abgetragen, in der
Achsenbeschriftung muss daher $G/[G]$ stehen:

#+BEGIN_CENTER
\begin{tikzpicture}
  \draw[->,thick] (0, 0) -- (0, 4);
  \node[rotate=90] at (-0.3, 2) {\footnotesize $v^2\,/\,\unit{(\metre\per\second)\squared}$} ;
  \draw[->,thick] (0, 0) -- node[below] {\footnotesize $p\,/\,\unit{\kilo\pascal}$} (6, 0) ;
  
  \draw (0, 0.5) -- (5, 3);
  \foreach \p in {(2,1.5), (3,2.0), (4,2.5)}{
    \draw[fill] \p circle[radius=1.5pt] ;
  }
\end{tikzpicture}
#+END_CENTER

*** Tabellenköpfe und Koordinatenachsen
**** Koordinatenachsen

Auf den Achsen wird die Maßzahl $\{G\}$ abgetragen, in der
Achsenbeschriftung muss daher $G/[G]$ stehen:

#+BEGIN_CENTER
#+BEGIN_EXPORT latex
\begin{tikzpicture}
  \begin{axis}[
    width=110mm,
    height=62mm,
    ymin=94760,
    ymax=94790,
    xlabel={$p\,/\,\unit{\kilo\pascal}$},
    ylabel={$v^2\,/\,\unit{(\metre\per\second)\squared}$},
    scaled y ticks = false,
    ymajorgrids=true,
    yticklabel={$\pgfmathprintnumber[fixed,1000 sep={\,},precision=0]{\tick}$},
    ]
    \addplot[
    mark=triangle*,
    black,
    mark options={fill=THKred,draw=THKred},
    ] coordinates {
      (50,94767) (70,94772) (135,94784)
    };
  \end{axis}
\end{tikzpicture}
#+END_EXPORT
#+END_CENTER


** weitere Einheiten

*** nicht-metrische Einheiten

Welche Währung wurde in Britannien um 52 v.Chr. verwendet?

\pause\bigskip

#+Attr_LaTeX: width=\textwidth
[[file:Briten_Geld.jpg]]
#+LaTeX: \src{R. Goscinny, A. Uderzo}{1966}


*** Umrechnung von Einheiten -- Übung 1

Paul Watzlawick schreibt in seiner in /Gebrauchsanweisung für Amerika/:

#+begin_quote
\small
Der Benzinverbrauch eines Wagens wird in Amerika in /miles
per gallon/ angegeben, und wenn Sie das jeweils in /Kilometer
pro Liter/ umrechnen wollen, wünsche ich Ihnen viel Glück.
#+end_quote

\medskip
Ein Hummer H2SUT soll \SI{12}{mpg} schaffen.
# \includegraphics{Hummer2SUT} 
#+LaTeX: \mbox{\scriptsize(\url{https://www.caranddriver.com/reviews/hummer-h2-sut-short-take-road-test})}.

Versuchen Sie Ihr Glück!

\medskip
($\SI{1}{mi} = \SI{1609.344}{\metre}$, \qquad $\SI{1}{ga} = \SI{3.78541178}{\litre}$)

\pause\bigskip
**** Lösung
\[
  \SI{12}{mpg} 
    \approx \frac{\SI{12}{mi}}{\SI{1}{ga}} 
      \cdot\frac{\SI{1610}{\metre}}{\SI{1}{mi}}
      \cdot\frac{\SI{1}{ga}}{\SI{3.79}{\litre}}
    \approx \SI{5100}{\metre\per\litre}
\]
Auf \SI{100}{\kilo\metre} verbraucht das Fahrzeug 
$\displaystyle\frac{\SI{100}{\kilo\metre}}{\SI{5100}{\metre\per\litre}} 
\approx \SI{19.6}{\litre}$
Kraftstoff!

*** Umrechnung von Einheiten -- Übung 2

Paul Watzlawick schreibt weiter:

#+BEGIN_QUOTE
\small
Der Reifendruck wird mit /pounds per square inch/ (Pfund pro
Quadratzoll) angegeben, und jeder Versuch, dies mit unseren
Atmosphären in Bezug zu bringen, ist meines Erachtens
verlorene Mühe.
#+END_QUOTE

\medskip
Da mag er recht haben; trotzdem finden Sie in einer amerikanischen Zeitschrift 
die Angabe /A systolic blood pressure of \SI{3}{psi} is not good/,
#+LaTeX: \hfill\mbox{\tiny(\url{https://www.vaughns-1-pagers.com/medicine/blood-pressure.htm})}.

1. Rechnen Sie den Druck (/Kraft/ durch /Fläche/) in SI-Einheiten
   ($\SI{1}{\pascal}=\SI{1}{\newton\per\square\metre}$) um.
2. Ärzte geben den Blutdruck in /Millimeter Quecksilbersäule/
   an. Welche Informationen brauchen Sie für die Umrechnung?

*** Umrechnung von Einheiten -- Übung 2 (Lösung)
**** 1. Druckangaben in /pound per square inch/
\begin{align*}
  \SI{1}{pd} &= \SI{4.44833}{\newton} \qquad
  \SI{1}{in} = \SI{0.0254}{\metre}   \\
  \SI{3}{psi} &\approx \frac{\SI{3}{pd}}{(\SI{1}{in})^2}
                 \cdot \frac{\SI{4.45}{\newton}}{\SI{1}{pd}}
                 \cdot \left(\frac{\SI{1}{in}}{\SI{0.0254}{\metre}}\right)^2 
             \approx \SI{21000}{\newton\per\square\metre}
               = \SI{21}{\kilo\pascal}
\end{align*}

*** Umrechnung von Einheiten -- Übung 2 (Lösung)
**** 2. Druckangaben in mm Hg
\begin{align*}
  p &= \frac{F}{A}    & \text{Druck}  &= \text{Kraft}/\text{Fläche} \\
  F &= m\cdot a       & \text{Kraft}  &= \text{Masse}\cdot\text{Beschleunigung} \\
  m &= \varrho\cdot V & \text{Masse}  &= \text{Dichte}\cdot\text{Volumen} \\
  V &= A \cdot h      & \text{Volumen}&= \text{Grundfläche}\cdot\text{Höhe}
\end{align*}

\begin{equation*}
  p = \frac{F}{A} = \frac{m\cdot a}{A} 
                  = \frac{\varrho\cdot A \cdot h \cdot a}{A}
                  = \varrho\cdot h \cdot a
\end{equation*}

\smallskip Wir benötigen den Wert der Erdbeschleunigung $a = g \approx
\SI{9.81}{\metre\per\second\squared}$ und die Dichte von Quecksilber
unter Normalbedingungen $\varrho \approx
\SI{13.5459}{\gram\per\centi\metre\cubed}$ bei \SI{293.15}{\kelvin}.

**** COMMENT Überlegungen zum Druck

- Was tun Sie, wenn Sie im Eis einbrechen oder ihr Fuß im Moor den
  Grund verliert?

*** Umrechnung von Einheiten -- /Mars Climate Orbiter/

**** MCO                                              :B_ignoreheading:BMCOL:
     :PROPERTIES:
     :BEAMER_col: 0.5
     :BEAMER_env: ignoreheading
     :END:

#+Attr_LaTeX: :height 4cm
[[file:mars_climate_orbiter.jpg]]

**** MCO                                                              :BMCOL:
     :PROPERTIES:
     :BEAMER_col: 0.5
     :BEAMER_env: ignoreheading
     :END:
- Kosten: 125 Millionen Dollar
- Start 11.12.1998
- Ziele:
  + Erreichen einer Umlaufbahn um den Mars
  + Kartographierung der Oberfläche
  + Relaisstation für den /Mars Polar Lander/

Der /Mars Climate Orbiter/ verglühte allerdings in der Marsatmosphäre.

*** Ursache für den Absturz

#+BEGIN_QUOTE
\small

1999: A disaster investigation board reports that NASA's Mars Climate
Orbiter burned up in the Martian atmosphere because engineers failed
to convert units from English to
metric. 
#+END_QUOTE

#+LaTeX: \par\bigskip\src{\url{https://www.wired.com/2010/11/1110mars-climate-observer-report/}}{2010}


**** Folgerung Einheiten                                       :B_alertblock:
     :PROPERTIES:
     :BEAMER_env: alertblock
     :END:

Die Angabe einer physikalischen Größe ohne Einheiten ist wertlos.

(Ausnahme: Die Maßzahl ist 0.)


** Primärgrößen, Gibbs-Funktion

*** Intensive und extensive Größen

**** Gedankenexperiment                                             :B_block:
     :PROPERTIES:
     :BEAMER_env: block
     :END:

In einer Badewanne befinden sich \SI{20}{\litre} Wasser mit einer
Temperatur von \SI{25}{\celsius}. Dazu werden noch einmal
\SI{20}{\litre} \SI{25}{\celsius} warmes Wasser gegeben.

1) Wieviel Wasser ist in der Wanne?
2) Welche Temperatur hat es?

\pause
**** Extensive und intensive Größen                         :B_ignoreheading:
     :PROPERTIES:
     :BEAMER_env: ignoreheading
     :END:

Das Volumen ist eine *extensive* (mengenartige) Größe.

Die Temperatur ist eine *intensive* Größe. 

\pause\bigskip
**** Achtung                                                   :B_alertblock:
     :PROPERTIES:
     :BEAMER_env: alertblock
     :END:
Addieren darf man nur mengenartige Größen!


*** Primärgrößen

Die folgenden Größen sind /mengenartig/ und können /bilanziert/ (addiert) werden:

\pause
**** Spalte 1                                                      :B_column:BMCOL:
     :PROPERTIES:
     :BEAMER_col: 0.4
     :BEAMER_env: column
     :END:
- Energie/Wärmemenge
- Entropie
- Impuls
- Drehimpuls

**** Spalte 2                                                      :B_column:
     :PROPERTIES:
     :BEAMER_col: 0.4
     :BEAMER_env: column
     :END:
- Stoffmenge
- Masse
- Ladung

\pause\medskip
**** Sonderfall Volumen:
Bei konstanter Dichte ist auch das /Volumen/ mengenartig.


*** Zusammenhang zwischen Primärgrößen (Stoff)

#+BEGIN_CENTER
#+BEGIN_EXPORT latex
\begin{tikzpicture}[xscale=3.5, yscale=2.5]
  \node[mengenartig] (M) at (-1,  1) {Masse \\ $m$}        ;
  \node[mengenartig] (Z) at ( 1,  1) {Teilchenzahl \\ $N$} ;
  \node[mengenartig] (V) at (-1, -1) {Volumen \\ $V$}      ;
  \node[mengenartig] (S) at ( 1, -1) {Stoffmenge \\ $\nu$} ;

  \draw[latex-latex] (M) edge node[intensive] {Teilchenmasse $m_1$} (Z) ;
  \draw[latex-latex] (M) edge node[intensive,rotate=90] {Dichte $\rho$} (V) ;
  \draw[latex-latex] (M) edge node[intensive,rotate=-35.54] {molare Masse $M$}    (S) ; % atan2(xscale, yscale)
  \draw[latex-latex] (Z) edge node[intensive,rotate=-90]  {Avogadro-Konstante $N_A$} (S) ;
  \draw[latex-latex] (V) edge node[intensive]       {molares Volumen $V_m$}    (S) ;
\end{tikzpicture}
#+END_EXPORT
#+END_CENTER

*** Zusammenhang zwischen Primärgrößen (Ladung)

#+BEGIN_CENTER
#+BEGIN_EXPORT latex
\begin{tikzpicture}[xscale=3.5, yscale=2.5]
  \node[mengenartig] (Z) at (-1,  1) {Teilchenzahl \\ $N$} ;
  \node[mengenartig] (S) at (-1, -1) {Stoffmenge \\ $\nu$};
  \node[mengenartig] (Q) at ( 1,  0) {Ladung \\ $Q$};

  \draw[latex-latex] (Z) edge node[intensive,rotate=-20.1]  {Elementarladung $e$} (Q) ;
  \draw[latex-latex] (S) edge node[intensive,rotate= 20.1]  {Faraday-Konstante $F$} (Q) ;
\end{tikzpicture}
#+END_EXPORT
#+END_CENTER

*** Übung – Zusammenhang zwischen Primärgrößen

#+BEGIN_CENTER
#+BEGIN_EXPORT latex
\begin{tikzpicture}[xscale=2.2, yscale=1.5]
  \node[mengenartig] (M) at (-1,  1) {Masse \\ $m$}        ;
  \node[mengenartig] (Z) at ( 1,  1) {Teilchenzahl \\ $N$} ;
  \node[mengenartig] (V) at (-1, -1) {Volumen \\ $V$}      ;
  \node[mengenartig] (S) at ( 1, -1) {Stoffmenge \\ $\nu$} ;
  \node[mengenartig] (Q) at ( 3,  0) {Ladung \\ $Q$}       ;

  \draw[latex-latex] (M) edge node[intensive] {$m_1$} (Z) ;
  \draw[latex-latex] (M) edge node[intensive, right] {$\rho$} (V) ;
  \draw[latex-latex] (M) edge node[intensive] {$M$}    (S) ; % atan2(xscale, yscale)
  \draw[latex-latex] (Z) edge node[intensive, right] {$N_A$} (S) ;
  \draw[latex-latex] (V) edge node[intensive] {$V_m$}    (S) ;

  \draw[latex-latex] (Z) edge node[intensive] {$e$} (Q) ;
  \draw[latex-latex] (S) edge node[intensive] {$F$} (Q) ;
\end{tikzpicture}
#+END_EXPORT
#+END_CENTER

**** Faraday-Konstante :B_block:
     :PROPERTIES:
     :BEAMER_env: block
     :END:
\small

1. Berechnen Sie die Faraday-Konstante aus der Avogadro-Konstanten $N_A \approx \qty{6.022e23}{\per\mole}$
   und der Elementarladung $e \approx \qty{1.602e-19}{\coulomb}$.
2. Wieviele Nachkommastellen sollten Sie beim Ergebnis angeben? Warum?

*** Potenzial und Strom

- Den Transport einer mengenartigen Größe nennt man /Strom/.
- Zu jeder mengenartigen Größe gibt es ein /Potenzial/ (intensive Größe).
  Die /Potenzialdifferenz/ verursacht den Strom.

#+ATTR_LATEX: :booktabs
#+BEGIN_CENTER
#+BEGIN_EXPORT latex
\footnotesize
\begin{tabular}{@{}llll@{}}
Menge & Potenzial & Strom & Energie- \\
      &           &       & änderung \\
\midrule
Masse  \(m\) & Gravitationspotenzial &  Massenstrom & $m\cdot g\cdot \Delta h$ \\
 & (Höhendifferenz $\Delta h$) &  & \\
Volumen \(V\) & Druckdifferenz \(\Delta p\) & Volumenstrom & \(V \cdot \Delta p\)\\
Impuls \(p\) & Geschwindigkeitsdifferenz \(\Delta v\) & Impulsstrom (Kraft) & $p \cdot \Delta v$ \\
Ladung \(Q\) & elektrisches Potenzial \(\Delta U\) & elektrischer Strom & \(Q\cdot \Delta U\)\\
Entropie \(S\) & Temperaturdifferenz  \(\Delta T\) & Wärmestrom & \(S\cdot \Delta T\)\\
Stoffmenge \(\nu\) & chemisches Potenzial \(\mu\) & Stoffmengenstrom & \(\nu\cdot \Delta\mu\)\\
 & (Konzentrationsgradient) &  & \\
\end{tabular}
#+END_EXPORT

# | Menge            | Potenzial                            | Strom              | Energie              |
# |------------------+--------------------------------------+--------------------+----------------------|
# | Masse  $m$       | Gravitationspotenzial                | Massenstrom        |                      |
# |                  | (Höhendifferenz)                     | Volumenstrom       |                      |
# | Volumen $V$      | Druckdifferenz $\Delta p$            |                    | $V \cdot \Delta p$   |
# | Impuls $p$       | Geschwindigkeitsdifferenz $\Delta v$ | Impulsstrom (Kraft) |                      |
# | Ladung $Q$       | elektrisches Potenzial $\Delta U$    | elektrischer Strom | $Q\cdot \Delta U$    |
# | Entropie $S$     | Temperaturdifferenz  $\Delta T$      | Wärmestrom         | $S\cdot \Delta T$    |
# | Stoffmenge $\nu$ | chemisches Potenzial $\mu$           | Stoffmengenstrom   | $\nu\cdot \Delta\mu$ |
# |                  | (Konzentrationsgradient)                                      |                    |                      |
#+END_CENTER

*** COMMENT Übung
Wie hängen die Einheiten der mengenartigen Größe und des zugehörigen
Potenzials zusammen?


*** Strom und Antrieb

**** Strom

- Änderung einer mengenartigen Größe

**** Antrieb

- Die Differenz (Potenzial) einer intensiven Größe ist immer mit dem Strom der zugehörigen
  mengenartigen Größe verbunden.

- Die Differenz der intensiven Größe kann als /Ursache/ für den Strom der zugehörigen 
  mengenartigen Größe verstanden werden.

*** Potenzialdifferenzen

**** Potenzialdifferenzen                                         :B_example:
     :PROPERTIES:
     :BEAMER_env: example
     :END:

- Ein Tropfen Tinte in einem Wasserglas (Konzentrationsdifferenz, /chemische Potenzialdifferenz/) 
  führt zu einem /Stoffmengenstrom/ (Konzentrationsausgleich). 
- Ein Unterschied im /Gravitationspotenzial/ (Höhe) führt zu einem /Massenstrom/.
- Eine /Temperaturdifferenz/ treibt den /Entropiestrom/ (Wärmetransport) an.
- Eine /elektrische Potenzialdifferenz/ (Spannung) führt zur Bewegung von Ladungen (el. Strom).
- Eine Differenz in der /Geschwindigkeit/ führt zu einem /Impulsstrom/ (Kraft).

*** Energieerhaltung

**** Gibbssche Fundamentalform

\begin{align*}
  \frac{dE}{dt} &= \vec v\cdot \frac{d \vec p}{dt}
                + \vec \omega \cdot \frac{d \vec L}{dt}
                + p \cdot \frac{d V}{dt}
                + \phi \cdot \frac{dQ}{dt}
                + T \cdot \frac{dS}{dt}
                + \mu \cdot \frac{dn}{dt}
                + \cdots
                = 0\\
  \Delta E      &= \vec v\cdot \Delta\vec p
                + \vec \omega \cdot \Delta \vec L
                + p \cdot \Delta V
                + \phi \cdot \Delta Q
                + T \cdot \Delta S
                + \mu \cdot \Delta n
                + \cdots
\end{align*}

\bigskip
| $\vec v$      | Geschwindigkeit        | \qquad | $\vec p$ | Impuls     |
| $\vec \omega$ | Winkelgeschwindigkeit  |        | $\vec L$ | Drehimpuls |
| $p$           | Druck                  |        | $V$      | Volumen    |
| $\phi$        | elektrisches Potenzial |        | $Q$      | Ladung     |
| $T$           | Temperatur             |        | $S$      | Entropie   |
| $\mu$         | chemisches Potenzial   |        | $n$      | Stoffmenge |




** Messunsicherheiten

*** Messfehler

# https://www.physik.uni-jena.de/pafmedia/praktika/physikalisches-grundpraktikum/kurzfehlerrechung-kurzanleitung.pdf

Jede Messung ist fehlerbehaftet

- zufällige Fehler

- systematische Fehler

- grobe Fehler

*** zufällige Fehler

- Schwankungen einer Anzeite
- Rauschen der Messkette
- kleine Umwelteinflüsse 
  \newline z.\thinspace B. 
  (Luftströmungen, Erschütterungen beim Wiegen)
- \dots


\pause

**** zufälliger Fehler                                      :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:


- nicht vermeidbar
- mitteln sich bei Messwiederholungen aus


*** systematische Fehler

- fehlerhafte Kalibrierung des Messgerätes
- fehlerhafte Durchführung der Messung
- prinzipielle Unvollkommenheit der Messmethode

\pause  
**** systematisch                                           :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:


- Fehler geht immer in die gleiche Richtung, \newline
  (keine Verbesserung durch wiederholte Messung),
- lassen sich (nach Erkennen des Fehlers) abstellen


*** Angabe der Messungenauigkeit

**** COMMENT implizit

| Zahlenangabe | Untergrenze | Obergrenze  |
|--------------+-------------+-------------|
| \num{2.00}   | \num{1.995} | \num{2.005} |
| \num{2.0}    | \num{1.95}  | \num{2.005} |
| \num{2}      | \num{1.5}   | \num{2.5}   |
| \num{20}     | ?           | ?           |
|--------------+-------------+-------------|
| \num{2e1}    | \num{15} | \num{25}
| \num{2.0e1}  | \num{19.5}  | \num{20.5}
|-

**** implizit

#+begin_center
#+begin_export latex
\begin{tabular}{@{}SSS@{}}
\toprule
\multicolumn{1}{c}{Zahlenangabe} & 
\multicolumn{1}{c}{Untergrenze} & 
\multicolumn{1}{c}{Obergrenze}\\
\midrule
2.00 & 1.995 & 2.005 \\
2.0  & 1.95  & 2.005 \\
2    & 1.5   & 2.5   \\
20 & ? & ?\\
\midrule\pause
2e1   & 15   & 25    \\
2.0e1 & 19.5 & 20.5  \\
\bottomrule
\end{tabular}
#+end_export
#+end_center


*** Angabe der Messungenauigkeit

**** explizite Fehlergrenzen

#+begin_center
#+begin_export latex
\sisetup{uncertainty-mode=separate}
\begin{tabular}{@{}SSS@{}}
\toprule
\multicolumn{1}{c}{Zahlenangabe} & 
\multicolumn{1}{c}{Untergrenze} & 
\multicolumn{1}{c}{Obergrenze}\\
\midrule
2.00 +- 0.005 & 1.995 & 2.005 \\
2.0 +- 0.05  & 1.95  & 2.005 \\
2 +- 0.5  & 1.5   & 2.5   \\
\midrule\pause
20 +- 5   & 15   & 25    \\
20 +- 0.5 & 19.5 & 20.5  \\
\bottomrule
\end{tabular}
#+end_export
#+end_center

*** Angabe der Messungenauigkeit

**** explizite Fehlergrenzen (kompakte Schreibweise)

#+begin_center
#+begin_export latex
\sisetup{uncertainty-mode=compact}
\begin{tabular}{@{}SSS@{}}
\toprule
\multicolumn{1}{c}{Zahlenangabe} & 
\multicolumn{1}{c}{Untergrenze} & 
\multicolumn{1}{c}{Obergrenze}\\
\midrule
2.00 +- 0.005 & 1.995 & 2.005 \\
2.0 +- 0.05  & 1.95  & 2.005 \\
2 +- 0.5  & 1.5   & 2.5   \\
\midrule\pause
20 +- 5   & 15   & 25    \\
20 +- 0.5 & 19.5 & 20.5  \\
\bottomrule
\end{tabular}
#+end_export
#+end_center

*** COMMENT Übung

Ist diese Angabe sinnvoll?

\[ \num{2.00 \pm 0.5} \]


*** Angabe der Messungenauigkeit

**** über ein Vertrauensintervall

Der Gehalt einer pharmazeutischen Zubereitung liegt mit einer
Wahrscheinlichkeit von \qty{95}{\percent} im Intervall 
\([\qty{29}{\milli\gram\per\milli\litre}, \qty{31}{\milli\gram\per\milli\litre}]\).

*** Signifikante Stellen

- \glqq Anzahl der zuverlässig bekannten Stellen mit Ausnahme der
  Nullen, die die Position des Kommas angeben\grqq (Tipler, 2019)


#+begin_center
#+begin_export latex
\begin{tabular}{@{}r@{\thinspace}lc@{}}
  \toprule
  \multicolumn{2}{c}{Größenangabe} & signifikante Stellen \\
  \midrule
  \num{2}&\unit{\metre} & 1 \\
  \num{0.567} & \unit{\kilo\gram} & 3 \\
  \num{1013} & \unit{\hecto\pascal} & 4 \\
  \num{0.00056} & \unit{\kilo\gram} & 2 \\
  \num{3600} & \unit{\second}       & 2 \\
  \num{3600.0} & \unit{\second}      & 5 \\
  \bottomrule
\end{tabular}
#+end_export
#+end_center



*** Rechnen mit fehlerbehafteten Größen

Beim Rechnen mit fehlerbehafteten physikalischen Größen ist die
Präzision des Ergebnisses höchstens so hoch wie die der Größe, die mit
der schwächsten Präzision in das Ergebnis einfließt:

\bigskip
**** Berechnung des Flächeninhalts eines Kreises
:PROPERTIES:
:BEAMER_env: example
:END:

Sie bestimmen den Durchmesser eines Kreises mit einem Lineal zu
\qty{235}{\milli\metre}.

Daraus berechnen Sie den Flächeninhalt nach $A = \pi r^2 = \pi d^2/4$
mit einem Taschenrechner zu \qty{43373.6135738}{\milli\metre\squared}.

Wie geben Sie das Ergebnis an?

\pause

*** Lösung

Da der Durchmesser mit drei signifikanten Stellen in die
Rechnung eingeht, geben Sie das Ergebnis auch nur auf drei
signifikanten Stellen *gerundet* an.

**** Ergebnis                                               :B_ignoreheading:
:PROPERTIES:
:BEAMER_env: ignoreheading
:END:
\[ A = \pi \frac{d^2}{4} = \qty{43400}{\milli\metre\squared}
                         = \qty{4.34e5}{\milli\metre\squared}
                         = \qty{434}{\centi\metre\squared} \]


**** Runden                                                    :B_alertblock:
:PROPERTIES:
:BEAMER_env: alertblock
:END:

Während der gesamten Rechnung arbeiten Sie mit allen verfügbaren Stellen.

Gerundet wird erst ganz am Schluss!
