# -*- coding: utf-8 -*-
#+TITLE:     PharmBasics (N) – Tabellenkalkulation
#+AUTHOR:    Prof. Dr. Richard Hirsch
#+EMAIL:     richard.hirsch@th-koeln.de
#+DATE:      Wintersemester 2022/23
#+LaTeX_HEADER: \institute{Pharmazeutische Chemie}

#+DESCRIPTION: Richard's lectures on statistics
#+KEYWORDS: 
#+LANGUAGE:  de

* Beamer configuration                                             :noexport:

#+startup: beamer
#+OPTIONS: H:3

** Latex

#+LaTeX_CLASS: lecture
#+LaTeX_CLASS_OPTIONS: [ngerman]

#+LaTeX_HEADER: \usepackage{babel}
#+LaTeX_HEADER: \usepackage{tikz}

#+LaTeX_HEADER: \sisetup{locale=DE}
#+LaTeX_HEADER: \graphicspath{{assets/images/}}

#+LaTeX_HEADER: \newcommand{\entspr}{\mathrel{\accentset{\wedge}{=}}}
#+LaTeX_HEADER: \newcommand{\src}[2]{\par\vspace{-7mm}\hfill{\scriptsize\textsl{#1}~(#2)}}


* Tabellenkalkulation

** Tabellenkalkulationsprogramme

*** Tabellenkalkulationsprogramme

Vielzweckprogramme

- Dateneingabe,
- Datenspeicherung,
- Auswertung,
- Visualisierung

\pause

Das macht das Arbeit mit Tabellenkalkulationsprogrammen bequem, aber auch sehr gefährlich!

**** Risiken
\pause
- Kontamiation der Rohdaten
- Datenverlust

Nach einer Analyse von Panko 
